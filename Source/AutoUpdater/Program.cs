﻿using System;
using System.IO;
using WUApiLib;

namespace AutoUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            DateTime compileDate= new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).LastWriteTime;
            string displayableVersion = $"{version} ({compileDate})";
            string logFile = "Updates.log";
            DateTime programStartTime = DateTime.Now;

            //check for updates

            Console.WriteLine("\nRunning AutoUpdater version: "+displayableVersion+"\n");
            Console.WriteLine("(1\\3) Performing check for applicable updates...");
            // Write the string to a file.
            File.WriteAllText(logFile, "Running AutoInstaller ;)" + Environment.NewLine);
            File.AppendAllText(logFile, "Performing check for applicable updates..." + Environment.NewLine);
            UpdateSession updateSession = new UpdateSession();
            ISearchResult searchResult = updateSession.CreateUpdateSearcher().Search("IsInstalled=0 and IsPresent=0 and IsHidden=0");
            int updatesCount = searchResult.Updates.Count;
            int i = 0;
            if (updatesCount.Equals(0))
            {
                Console.WriteLine("There are no updates left to install! :)");
                File.AppendAllText(logFile, "There are no updates left to install! :)" + Environment.NewLine);
                Environment.Exit(0);
            }
            Console.WriteLine("\n Found {0} updates:\n", updatesCount);
            string output = String.Format(" Found {0} updates:", updatesCount);
            File.AppendAllText(logFile, output + Environment.NewLine);
            foreach (IUpdate update in searchResult.Updates)
            {
                i++;
                output = String.Format(" {0}/{1} {2}", i, updatesCount, update.Title);
                File.AppendAllText(logFile, output + Environment.NewLine);
                Console.WriteLine(" {0}/{1} {2}", i, updatesCount, update.Title);
            }
            Console.WriteLine("\nSearching for updates took: {0} \n", (DateTime.Now - programStartTime).ToString(@"hh\:mm\:ss"));
            
            //download updates
            i = 0;
            UpdateDownloader downloader = updateSession.CreateUpdateDownloader();
            UpdateCollection updatesToDownload = new UpdateCollection();
            Console.WriteLine("(2\\3) Downloading {0} updates:", updatesCount);
            output = String.Format("(2\\3) Downloading {0} updates:", updatesCount);
            File.AppendAllText(logFile, output + Environment.NewLine);
            foreach (IUpdate update in searchResult.Updates)
            {
                i++;
                Console.WriteLine(" {0}/{1} {2}", i, updatesCount, update.Title);
                output = String.Format(" {0}/{1} {2}", i, updatesCount, update.Title);
                File.AppendAllText(logFile, output + Environment.NewLine);
                updatesToDownload.Add(update);
                downloader.Updates = updatesToDownload;
                downloader.Download();
                updatesToDownload.Clear();
            }
            Console.WriteLine(" Download finished!");
            File.AppendAllText(logFile, " Download finished!" + Environment.NewLine);
            
            //collect all downloaded updates & install downloaded updates
            IUpdateInstaller installer = updateSession.CreateUpdateInstaller();
            UpdateCollection updatesToInstall = new UpdateCollection();
            UpdateCollection singleUpdate = new UpdateCollection();
            foreach (IUpdate update in searchResult.Updates)
            {
                if (update.IsDownloaded)
                {
                    if (update.EulaAccepted == false)
                    {
                        update.AcceptEula();
                    }
                    updatesToInstall.Add(update);
                }
            }
            i = 0;
            Console.WriteLine("(3\\3) Installing {0} updates:", updatesToInstall.Count);
            output = String.Format("(3\\3) Installing {0} updates:", updatesToInstall.Count);
            File.AppendAllText(logFile, output + Environment.NewLine);
            foreach (IUpdate update in updatesToInstall)
            {
                i++;
                Console.WriteLine(" {0}/{1} {2}", i, updatesToInstall.Count, update.Title);
                output = String.Format(" {0}/{1} {2}", i, updatesToInstall.Count, update.Title);
                File.AppendAllText(logFile, output + Environment.NewLine);
                singleUpdate.Add(update);
                installer.Updates = singleUpdate;
                IInstallationResult installationRes = installer.Install();
            }
            Console.WriteLine("Finished installing updates!");
            File.AppendAllText(logFile, "Finished installing updates!" + Environment.NewLine);
            Environment.Exit(0);
        }
    }
}
